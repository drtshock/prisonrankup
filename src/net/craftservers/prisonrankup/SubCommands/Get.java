package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.PR;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Get extends SubCommand{

    public Get() {
        super("get");
    }

    @Override
    public void onCommand(Player player, Command cmd, String label, String[] args) {
        this.onCommand((CommandSender) player, cmd, label, args);
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("prisonrankup.get")) {
            sender.sendMessage(Lang.noPermissions);
            return;
        }
        if(args.length >= 1) {
            PRPlayer player = new PRPlayer(args[0]);
            String prefix = ConfigManager.getConfig().getString("Prefix");
            sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
            sender.sendMessage(ChatColor.AQUA + player.getName() + "'s Profile:");
            sender.sendMessage(ChatColor.AQUA + "Current balance: " + PR.getEconomy().getBalance(player.getName()));
            sender.sendMessage(ChatColor.AQUA + "Current rank: " + player.getCurrentRank().getName());
            sender.sendMessage(ChatColor.AQUA + "Next rank: " + player.getNextRank().getName());
            sender.sendMessage(ChatColor.AQUA + "UUID: " + player.getStringUUID());
            sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
        }else{
            Lang.description(sender);
        }
    }
}
