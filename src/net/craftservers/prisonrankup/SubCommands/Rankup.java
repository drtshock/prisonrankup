package net.craftservers.prisonrankup.SubCommands;

import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.Rank;
import net.craftservers.prisonrankup.Models.SubCommand;
import net.craftservers.prisonrankup.PR;
import net.craftservers.prisonrankup.Utils.AccessUtil;
import net.craftservers.prisonrankup.Utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Rankup extends SubCommand{

    String prefix;

    public Rankup() {
        super("rankup");
        prefix = Lang.prefix;
    }

    @Override
    public void onCommand(final Player p, Command cmd, String label, String[] args) {
        if(p.hasPermission("prisonrankup.rankup")) {
            PRPlayer player = new PRPlayer(p.getName());
            if(!player.canRankup()) {
                switch((AccessUtil.DenyReason) AccessUtil.data.get(p.getName())) {
                    case INSUFFICIENT_FUNDS:
                        String formatString = ConfigManager.getConfig().getString("not-enough-money").replaceAll("%rank%", player.getNextRank().getName()).replaceAll("%price%", player.getNextRank().getPriceString());
                        formatString = Lang.translateToColor(formatString);
                        p.sendMessage(formatString);
                        break;
                    case COOLDOWN:
                        PR main = PR.getPlugin();
                        String type = main.getConfig().getString("Time type");
                        switch(type.toLowerCase()) {
                            case "seconds":
                                p.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + main.getConfig().getDouble("Time Interval")  + " seconds!");
                                break;
                            case "hours":
                                p.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + main.getConfig().getDouble("Time Interval") / 360 + " hours!");
                                break;
                            case "minutes":
                                p.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + main.getConfig().getDouble("Time Interval") / 60 + " minutes!");
                                break;
                            default:
                                p.sendMessage(Lang.error("Config not defined properly!"));
                                break;
                        }
                        break;
                    default:
                        p.sendMessage(Lang.error("Error was generated, please enable debug mode to debug the issue!"));
                        break;
                }
                AccessUtil.data.remove(p.getName());
            }else if(player.getCurrentRank().equals(player.getNextRank())) {
                p.sendMessage(Lang.translateToColor(ConfigManager.getConfig().getString("Highest Rank MSG")));
            }else{
                Rank nextRank = player.getNextRank();
                PR.debug("Got his next available rank " + nextRank.getName());
                player.rankup();
                Bukkit.broadcastMessage(prefix + ChatColor.translateAlternateColorCodes('&', ConfigManager.getConfig().getString("Rankup BC Message"))
                        .replaceAll("%player%", player.getName())
                        .replaceAll("%rank%", nextRank.getName())
                        .replaceAll("%displayname%", p.getDisplayName())
                        .replaceAll("%prefix%", PR.getChat().getPlayerPrefix(p))
                        .replaceAll("%suffix%", PR.getChat().getPlayerSuffix(p)));
            }
        }else{
            p.sendMessage(ChatColor.DARK_RED + "You do not have permission to this command!");
        }
    }

    @Override
    public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        //Empty since Console cannot rankup, or any other entity than a player for that matter.
        sender.sendMessage("Only players can run this command!");
    }
}
