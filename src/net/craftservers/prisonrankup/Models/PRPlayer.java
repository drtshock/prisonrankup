package net.craftservers.prisonrankup.Models;

import net.craftservers.prisonrankup.Listeners.TimeHandler;
import net.craftservers.prisonrankup.Listeners.UUIDUtil;
import net.craftservers.prisonrankup.Managers.ConfigManager;
import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.PR;
import net.craftservers.prisonrankup.Utils.AccessUtil;
import net.craftservers.prisonrankup.Utils.UUIDFetcher;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import static net.craftservers.prisonrankup.PR.*;

public class PRPlayer {

    private String name;
    private Rank currentRank;
    private Rank nextRank;
    private String uuid;
    private ConfigManager cm;

    public PRPlayer(String name) {
        cm = Manager.getConfigManager();
        this.name = name;
        if(getPlayer() == null) {
            uuid = UUIDFetcher.getUUIDFromName(name).replaceAll("-", "");
            UUIDUtil.uuidstorage.put(name, uuid);
        }else{
            if(!UUIDUtil.uuidstorage.containsKey(name)) {
                UUIDUtil.uuidstorage.put(name, getPlayer().getUniqueId().toString().replaceAll("-", ""));
            }
            uuid = UUIDUtil.uuidstorage.get(name);
            debug("Updated UUID through cache");
        }
        if(!exists()) {
            create();
            debug("Created a profile");
        }
        debug(cm.getUserConfig().getString("users." + uuid + ".group"));
        currentRank = new Rank(cm.getUserConfig().getString("users." + uuid + ".group"));
        debug("Updated currentRank: " + currentRank.toString());
        try{
            int location = currentRank.getLocation() + 1;
            String rankDat = ConfigManager.getConfig().getStringList("groups").get(location);
            nextRank = new Rank(rankDat.split(":")[0], location);
        }catch(IndexOutOfBoundsException e) {nextRank = currentRank;}
    }

    public boolean exists() {
        return cm.getUserSection().contains(uuid);
    }

    public void create() {
        if(!exists()) {
            cm.getUserConfig().createSection("users." + getStringUUID());
            if(ConfigManager.getConfig().getBoolean("Transfer ranks to profile")) {
                cm.getUserConfig().set("users." + getStringUUID() + ".group", getPermissionRank());
                return;
            }
            cm.getUserConfig().set("users." + getStringUUID() + ".group", ConfigManager.getConfig().getStringList("groups").get(0).split(":")[0]);
            cm.saveUserConfig();
            cm.reloadUserConfig();
        }
    }

    public String getPermissionRank() {
        String rank = "";
        for(String s : getPermission().getPlayerGroups(getPlayer())) {
            for(String str : Manager.getRankManager().getAllRanks()) {
                if(s.equalsIgnoreCase(str)) {
                    rank = s;
                    break;
                }
            }
        }
        return rank;
    }

    public Rank getNextRank() {return nextRank;}

    public String getStringUUID() {return uuid;}

    public Rank getCurrentRank() {return currentRank;}

    public String getName() {return name;}

    public Player getPlayer() {
        return Bukkit.getPlayer(name);
    }

    public PRPlayer rankup() {
        EconomyResponse er = PR.getEconomy().withdrawPlayer(name, getNextRank().getPrice());
        if(er.transactionSuccess()) {
            Permission perm = getPermission();

            debug("Removing the group from the player");
            perm.playerRemoveGroup((World) null, name, currentRank.getName());
            debug("Adding the group");
            perm.playerAddGroup((World) null, name, nextRank.getName());

            cm.getUserConfig().set("users." + getStringUUID() + ".group", null);
            String path = "users." + getStringUUID() + ".group";
            String value = nextRank.getName();
            cm.getUserConfig().set(path, value);
            debug("Updated config file");
            cm.saveUserConfig();
            cm.reloadUserConfig();

            if(ConfigManager.getConfig().getBoolean("Timed Requirement") && ConfigManager.getConfig().getBoolean("Interval on all ranks")){
                TimeHandler.cooldown.add(getPlayer());
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("PrisonRankup"), new Runnable(){

                    @Override
                    public void run() {
                        TimeHandler.cooldown.remove(getPlayer());
                    }

                }, ConfigManager.getConfig().getInt("Time Interval") * 20);
            }
        }
        return new PRPlayer(name);
    }

    public boolean canRankup() {
        boolean answer = !(TimeHandler.cooldown.contains(getPlayer())) && PR.getEconomy().getBalance(name) >= nextRank.getPrice();
        if(!answer && (TimeHandler.cooldown.contains(getPlayer()))) {
            AccessUtil.data.put(name, AccessUtil.DenyReason.COOLDOWN);
        }else if(!answer) {
            AccessUtil.data.put(name, AccessUtil.DenyReason.INSUFFICIENT_FUNDS);
        }
        return answer;
    }

    public PRPlayer setRank(Rank rank) {
        getPermission().playerRemoveGroup((World) null, name, currentRank.getName());
        getPermission().playerAddGroup((World) null, name, nextRank.getName());
        cm.getUserConfig().set("users." + getStringUUID() + ".group", null);
        cm.getUserConfig().set("users." + getStringUUID() + ".group", rank.getName());
        cm.saveUserConfig();
        cm.reloadUserConfig();
        return new PRPlayer(name);
    }

}
