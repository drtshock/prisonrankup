package net.craftservers.prisonrankup.Utils;

import net.craftservers.prisonrankup.PR;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Lang {

     /*
	 *                                                                                   *
	 *  This source is under the right of All Rights Reserved,                           *
	 *  You may not copy or edit this code or distribute it in any way without a license *
	 *  from Mazen K.                                                                    *
	 * Copyright � 2014 CraftServers. All Rights Reserved.                               *
	 *                                                                                   *
	 */

    public final String noPermission = ChatColor.RED  + "You do not have permission to this command";
    public final static String noPermissions = ChatColor.RED  + "You do not have permission to this command";
    public static String prefix = ChatColor.translateAlternateColorCodes('&', PR.getPlugin().getConfig().getString("Prefix")) + " ";

    public static String error(String error){
        return prefix + ChatColor.RED + "" + ChatColor.BOLD + error;
    }

    public static void description(CommandSender sender){
        sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
        sender.sendMessage("");
        sender.sendMessage(ChatColor.AQUA + "/rankup - " + ChatColor.GOLD + "Rankup to your wildest dreams!");
        sender.sendMessage(ChatColor.AQUA + "/rankup get [Player] - " + ChatColor.GOLD + "Retrieve a players profile");
        sender.sendMessage(ChatColor.AQUA + "/rankup stats [Rank] - " + ChatColor.GOLD + "Retrieve information about a rank specified");
        sender.sendMessage(ChatColor.AQUA + "/rankup set [Player] [Rank] - " + ChatColor.GOLD + "Set a player to a certain rank!");
        sender.sendMessage(ChatColor.AQUA + "/rankup create <group> <price> [index] - " + ChatColor.GOLD + "Create a rank from in-game!");
        sender.sendMessage(ChatColor.AQUA + "/rankup update - " + ChatColor.GOLD + "Update PrisonRankup to its latest version!");
        sender.sendMessage("");
        sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', prefix.replaceAll(" ", "") + "---------"));
    }

    public static String translateToColor(String str) {
        return ChatColor.translateAlternateColorCodes('&', str);
    }

}
