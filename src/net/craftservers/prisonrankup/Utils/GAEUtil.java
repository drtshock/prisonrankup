package net.craftservers.prisonrankup.Utils;

import net.craftservers.prisonrankup.Utils.Events.GroupAddEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class GAEUtil implements Listener{

    String[] args;

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String[] data = event.getMessage().split(" ");
        String command = data[0].replaceAll("/", "");
        StringBuilder sb = new StringBuilder("");
        for(int i = 1; i < data.length; i++) {
            sb.append(data[i]);
            sb.append("//");
        }
        Player p = event.getPlayer();
        args = sb.toString().split("//");
        boolean argsExists = (args.length == 0);
        switch(command.toLowerCase()) {
            case "changeplayer":
                if(!argsExists) {
                    switch(args[0].toLowerCase()) {
                        case "addsub":
                            if(checkArgs(3)) {
                                if(p.hasPermission("droxperms.players")) {
                                    callEvent(p, args[1], args[2]);
                                }
                            }
                            break;
                        case "setgroup":
                            if(checkArgs(3)) {
                                if(p.hasPermission("droxperms.players")) {
                                    callEvent(p, args[1], args[2]);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                break;
            case "manuadd":
                if(!argsExists) {
                    if(checkArgs(2)) {
                        if(p.hasPermission("groupmanager.manuadd")) {
                            callEvent(p, args[0], args[1]);
                        }
                    }
                }
                break;
            case "manuaddsub":
                if(!argsExists) {
                    if(checkArgs(2)) {
                        if(p.hasPermission("groupmanager.manuaddsub")) {
                            callEvent(p, args[0], args[1]);
                        }
                    }
                }
                break;
            case "pex":
                if(!argsExists) {
                    switch(args[0].toLowerCase()) {
                        case "user":
                            if(checkArgs(5)) {
                                if(args[2].equalsIgnoreCase("group")) {
                                    if(args[3].equalsIgnoreCase("add")){
                                        callEvent(p, args[1], args[4]);
                                    }else if(args[3].equalsIgnoreCase("set")) {
                                        callEvent(p, args[1], args[4]);
                                    }
                                }
                            }
                    }
                }
                break;
            case "playeraddgroup":
                if(!argsExists) {
                    if(checkArgs(2)) {
                        if(p.hasPermission("overpermissions.playeraddgroup")) {
                            callEvent(p, args[0], args[1]);
                        }
                    }
                }
                break;
            case "permissions":
                if(!argsExists) {
                    if(checkArgs(4)) {
                        if(args[0].equalsIgnoreCase("player")) {
                            if(args[1].equalsIgnoreCase("setgroup")) {
                                if(p.hasPermission("permissions.player.setgroup") || p.hasPermission("zpermissions.player.*") || p.hasPermission("zpermissions.player.setgroup")) {
                                    callEvent(p, args[2], args[3]);
                                }
                            }else if(args[1].equalsIgnoreCase("addgroup")) {
                                if(p.hasPermission("permissions.player.setgroup") || p.hasPermission("zpermissions.player.*") || p.hasPermission("zpermissions.player.addgroup")) {
                                    callEvent(p, args[2], args[3]);
                                }
                            }
                        }
                    }
                }
                break;
        }
    }

    public boolean checkArgs(int index) {
        return args.length >= index;
    }

    public void callEvent(Player p, String target, String newGroup) {
        Bukkit.getPluginManager().callEvent(new GroupAddEvent(p, target, newGroup));
    }

}
