package net.craftservers.prisonrankup.Listeners;

import net.craftservers.prisonrankup.PR;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

public class TimeHandler implements Listener {

    PR main = PR.getPlugin();
    int tid = 0;
    public static ArrayList<Player> cooldown = new ArrayList<>();

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        if(main.getConfig().getBoolean("Timed Requirement")){
            final Player player = event.getPlayer();
            cooldown.add(event.getPlayer());
            tid = main.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("PrisonRankup"), new Runnable(){

                @Override
                public void run() {
                    cooldown.remove(player);
                }

            }, main.getConfig().getInt("Time Interval") * 20);
        }
    }
}
