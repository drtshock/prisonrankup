package net.craftservers.prisonrankup.Listeners;

import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.PR;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinHandler implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if(!event.getPlayer().hasPlayedBefore()) {
            UUIDUtil.uuidstorage.put(event.getPlayer().getName(), event.getPlayer().getUniqueId().toString().replaceAll("-", ""));
            PRPlayer player = new PRPlayer(event.getPlayer().getName());
        }
        if(event.getPlayer().isOp() || event.getPlayer().hasPermission("prisonrankup.update")) {
            Player player = event.getPlayer();
            player.sendMessage("An update is available: " + PR.name + ", a " + PR.type + " for " + PR.version + " available at " + PR.link);
            player.sendMessage("Type /update if you would like to automatically update.");
        }
    }
}
